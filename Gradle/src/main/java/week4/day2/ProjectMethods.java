package week4.day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excel.ReadExcel;

public class ProjectMethods extends SeMethods  {
	
public String dataSheetName;	
@BeforeMethod
	public void login()
	{
	
	startApp("chrome", "http://leaftaps.com/opentaps/control/logout");
	WebElement element1 = locateElement("username");
	type(element1, "DemoSalesManager");
	WebElement pass1 = locateElement("password");
	type(pass1, "crmsfa");
	WebElement clk = locateElement("class", "decorativeSubmit");
	click(clk);
	WebElement lt1 = locateElement("linktext", "CRM/SFA");
	click(lt1);
	}

@DataProvider(name="qa")

public Object[][] getData() throws IOException{
	//return DataInputProvider.getSheet(dataSheetName);
	Object[][] data = ReadExcel.excel(dataSheetName);
	return data;
	
}
		
	

//@AfterMethod(groups= {"regression", "smoke", "sanity"})
/*@AfterMethod
public void close()
{
	closeBrowser();
}*/





}
