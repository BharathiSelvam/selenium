package testng;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.IAnnotationTransformer;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.annotations.ITestAnnotation;

import cucumber.api.java.lu.an;

public class LearnListener implements IAnnotationTransformer, IRetryAnalyzer{
int maxReTry = 1;
	@Override
	public void transform(ITestAnnotation annotation, Class testClass,
			Constructor testConstructor, Method testMethod) {
	
		if (!testMethod.getName().equals("editLead")) {
			annotation.setEnabled(false);
		}
		annotation.setInvocationCount(1);
		annotation.setRetryAnalyzer(this.getClass());
	}
	@Override
	public boolean retry(ITestResult result) {
		if (maxReTry<2) {
			maxReTry++;
			return true;			
		}
		return false;
	}

	

	
}
