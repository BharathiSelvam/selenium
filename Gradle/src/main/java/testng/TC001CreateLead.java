package testng;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week4.day2.ProjectMethods;




public class TC001CreateLead extends ProjectMethods{

	@BeforeTest
		public void setData() {
			testCaseName = "TC001_CreateLead";
			testDesc = "Create A new Lead";
			author = "gopi";
			category = "smoke";
		}
	
	@Test
	public void createLead() {
		
		WebElement cl = locateElement("linktext", "Create Lead");
		click(cl);
		type(locateElement("createLeadForm_companyName"), "TL");
		type(locateElement("createLeadForm_firstName"), "Bharathi");
		type(locateElement("createLeadForm_lastName"), "P");
		click(locateElement("class", "smallSubmit"));
	}
	
}












