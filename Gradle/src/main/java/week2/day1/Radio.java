package week2.day1;

public abstract class Radio {
	
	public void playStation() {
		System.out.println("PlayStation");
	}

	public abstract void changeBattery();
	
	}
