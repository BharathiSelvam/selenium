package week2.day1;

public class Television implements Entertainment{

		
	public void volume() {
		System.out.println("Increase the volume");
	}
	
	public void changeChannel() {
		System.out.println("Change the Channel");
	}
	
	public void tunner() {
		System.out.println("set the tunner");
	}

	@Override
	public void music() {
		System.out.println("Music");
		
	}

	@Override
	public void movies() {
		System.out.println("Movies");
		
	}

}
